const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");
const userRoutes = require("./routes/user.js")
const courseRoutes = require("./routes/course.js")


const app = express();

// DB connection
mongoose.connect("mongodb+srv://admin:admin123@zuitt.8qw2w8u.mongodb.net/Course-Booking-API?retryWrites=true&w=majority" , {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
// checks if connection to DB is ok
mongoose.connection.once("open", () => console.log("Now connected to MongoDB Atlas"))




app.use(cors());

app.use(express.json());

app.use(express.urlencoded({extended: true}));

// inizializing the routes
app.use("/users" , userRoutes);

app.use("/courses" , courseRoutes);




app.listen(process.env.PORT || 4000 , () => {
	console.log(`API is now online on port ${process.env.PORT || 4000}`);
})